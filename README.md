# Chat_Bot_Demo
This a chat bot example for all the medical websites/apps and showcases its excellent ability to answer even some of the most complicated medical questions in detail. It can also suggest some remedies for common infections and body problems.


## Getting started

## Steps to run

-> Open the backend and frontend in individual terminals

## Backend 

 1. Install all the dependencies/packages (pip install flask flask_cors torch torchaudio torchvision transfromers)
 2. Run "python app.py"

## Frontend
 
 1. Run "npm i"
 2. Run "npm install axios"
 3. Run "npm start"