import React, { useState, useRef, useEffect } from 'react';
import axios from 'axios';
import './App.css';
import botIcon from './bot.png'; // Import your bot icon image
import userIcon from './user.png'; // Import your user icon image

function App() {
  const [userInput, setUserInput] = useState('');
  const [botResponses, setBotResponses] = useState([]);
  const [loading, setLoading] = useState(false);

  const chatContainerRef = useRef(null);

  const sendMessage = async () => {
    if (!userInput.trim()) return; // Ignore empty messages

    const newUserMessage = { type: 'user', text: userInput };
    setBotResponses((prevResponses) => [...prevResponses, newUserMessage]);

    setLoading(true);
    setUserInput('');
    try {
      const response = await axios.post('http://localhost:5000/chat', { user_input: userInput });
      const newBotMessage = { type: 'bot', text: response.data.response };

      setBotResponses((prevResponses) => [...prevResponses, newBotMessage]);
    } catch (error) {
      console.error('Error sending message:', error);
      const errorMessage = { type: 'bot', text: 'Error occurred' };
      setBotResponses((prevResponses) => [...prevResponses, errorMessage]);
    } finally {
      setLoading(false);
      setUserInput('');
    }
  };

  useEffect(() => {
    // Scroll chat container to bottom when new message is added
    chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
  }, [botResponses]);

  return (
    <div className="App">
      <h1 className="specific-heading">Chatbot</h1>
      <div className="scroll-bar">
        <div className="chat-container" ref={chatContainerRef}>
          {botResponses.map((message, index) => (
            <div key={index} className={`message ${message.type}`}>
              {message.type === 'bot' && (
                <div className="avatar">
                  <img src={botIcon} alt="Bot" className="avatar-image" />
                </div>
              )}
              <div className="message-content">
                <p>{message.text}</p>
                {message.type === 'user' && (
                  <div className="avatar-right">
                    <img src={userIcon} alt="User" className="avatar-image" />
                  </div>
                )}
              </div>
            </div>
          ))}
          {loading && (
            <div className="message bot">
              <div className="avatar">
                <img src={botIcon} alt="Bot" className="avatar-image" />
              </div>
              <div className="message-content">
                <p>Loading...</p>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className="user-input">
        <input
          type="text"
          value={userInput}
          onChange={(e) => setUserInput(e.target.value)}
          placeholder="Type a message..."
          onKeyPress={(e) => {
            if (e.key === 'Enter') sendMessage();
          }}
        />
        <button onClick={sendMessage} disabled={loading}>
          {loading ? 'Wait...' : 'Send'}
        </button>
      </div>
    </div>
  );
}

export default App;