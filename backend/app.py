from flask import Flask, request, jsonify
from threading import Thread
from flask_cors import CORS
import torch
from transformers import GPT2LMHeadModel, GPT2Tokenizer

app = Flask(__name__)
CORS(app)
# Load the AI model and tokenizer
path = "Mohammed-Altaf/Medical-ChatBot"
device = "mps"  # Use CPU since Flask typically runs on CPU
tokenizer = GPT2Tokenizer.from_pretrained(path)
model = GPT2LMHeadModel.from_pretrained(path).to(device)

# Chat prompt template
prompt_input = (
    "The conversation between human and AI assistant.\n"
    "[|Human|] {input}\n"
    "[|AI|]"
)

# Handle POST requests to /api/chat
@app.route('/chat', methods=['POST'])
def chat():
    user_input = request.json['user_input']
    input_sent = user_input
    print("Entered api"+input_sent)
    sentence = prompt_input.format(input=input_sent)
    inputs = tokenizer(sentence, return_tensors="pt").to(device)

    with torch.no_grad():
        beam_output = model.generate(
            **inputs,
            min_length=50,
            max_length=100,
            num_beams=3,
            repetition_penalty=1.2,
            early_stopping=True,
            eos_token_id=tokenizer.eos_token_id
        )
    conversation = tokenizer.decode(beam_output[0], skip_special_tokens=True)
    lines = conversation.split('\n')

    # Iterate through the lines to find the AI response
    ai_response = None
    for line in lines:
        if '[|AI|]' in line:
            ai_response = line.split('[|AI|]')[1].strip()
            break

    print(ai_response)
    
    # return response_text
    # response = chatbot_response(user_input)

    return jsonify({'response': ai_response})


if __name__ == '__main__':
    app.run(host='0.0.0.0',port='5000',debug=True)
